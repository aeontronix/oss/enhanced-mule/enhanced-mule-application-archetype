import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

Path projectPath = Paths.get(request.outputDirectory, request.artifactId)
Properties properties = request.properties
String apiSpecId = properties.get("apiSpecId")
String apiSpecType = properties.get("apiSpecType")
String apiSpecExt = properties.get("apiSpecExt")
String emProperties = properties.get("emProperties")
String restEnabled = properties.get("rest")
String apiSpecInProject = properties.get("apiSpecInProject")
String vagrantfile = properties.get("vagrantfile")
Path mulePath = projectPath.resolve("src/main/mule")
Path resPath = projectPath.resolve("src/main/resources")
Path apiPath = projectPath.resolve("src/main/resources/api")

if (!restEnabled.equals("yes")) {
    println "Deleting the http-router file"
    Files.deleteIfExists mulePath.resolve("http-router.xml")
}

if (emProperties.equals("yes")) {
    println "Deleting the config files file"
    Files.deleteIfExists resPath.resolve("config.yaml")
}

if (apiSpecInProject.equals("no") || !apiSpecType.equals("raml")) {
    Files.deleteIfExists apiPath.resolve(apiSpecId + ".raml")
}

if (apiSpecInProject.equals("no") || !(apiSpecType.equals("oas") && apiSpecExt.equals("yaml") )) {
    Files.deleteIfExists apiPath.resolve(apiSpecId + ".yaml")
}

if (apiSpecInProject.equals("no") || !(apiSpecType.equals("oas") && apiSpecExt.equals("json") )) {
    Files.deleteIfExists apiPath.resolve(apiSpecId + ".json")
}

if (vagrantfile.equals("no") ) {
    Files.deleteIfExists apiPath.resolve("Vagrantfile")
}
