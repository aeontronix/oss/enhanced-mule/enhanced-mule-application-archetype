# myapp

*This application incorporates [enhanced-mule-tools](https://enhanced-mule-tools.aeontronix.com/) (EMT) to automate
various aspects of anypoint provision and provide various capabilities not included in the standard mule maven plugin.
