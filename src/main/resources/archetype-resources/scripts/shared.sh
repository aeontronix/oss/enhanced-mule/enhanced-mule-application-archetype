#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
cryptprop() {
    GROUPID=com.myorg
    MKEYFILE="${symbol_dollar}{HOME}/.anypoint/mulekey.${symbol_dollar}{GROUPID}.txt"
    DIR="${symbol_dollar}( cd "${symbol_dollar}( dirname "${symbol_dollar}{BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
    if [[ "${symbol_dollar}{MULE_KEY}X" == "X" ]]; then
        if [[ -f ${symbol_dollar}{MKEYFILE} ]]; then
            MKEY=`cat ${symbol_dollar}{MKEYFILE}`
        else
            echo "Enter mule key"
            read
            MULE_KEY=${symbol_dollar}{REPLY}
        fi
    fi

    mkdir -p ~/.anypoint/
    if [[ ! -f ~/.anypoint/secure-properties-tool.jar ]]; then
        echo "Downloading secure properties tools"
        curl -s https://docs.mulesoft.com/downloads/mule-runtime/4.2/secure-properties-tool.jar -o ~/.anypoint/secure-properties-tool.jar
    fi

    java -cp ~/.anypoint/secure-properties-tool.jar com.mulesoft.tools.SecurePropertiesTool file ${symbol_dollar}1 AES CBC "${symbol_dollar}{MULE_KEY}" ${symbol_escape}
    ${symbol_dollar}{DIR}/../src/main/resources/config-secure.yaml ${symbol_dollar}{DIR}/../src/main/resources/config-secure.enc.yaml --use-random-iv
    mv -f ${symbol_dollar}{DIR}/../src/main/resources/config-secure.enc.yaml ${symbol_dollar}{DIR}/../src/main/resources/config-secure.yaml
}
