#!/bin/bash
set -e
mvn clean install
rm -rf target/atestproject
mkdir -p target/atestproject
cd target/atestproject
mvn -B archetype:generate -DarchetypeGroupId=com.aeontronix.enhancedmule -DarchetypeArtifactId=enhanced-mule-archetype \
-DarchetypeVersion=1.0.0-beta7-SNAPSHOT -DorgId=d002eff8-84a5-452f-bce6-145f4a688311 -Dpackage=com.mycompany \
-DgroupId=com.mycompany -DartifactId=atestproject -Drest=yes \
-DapiSpecId=atestproject-spec \
-Djsonlogging=true -DapiSpecInProject=no
